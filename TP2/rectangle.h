#include <iostream>
#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Point.h"

using namespace std;

class Rectangle
{
private:
    int longueur, largeur;
    Point coinSuperieurGauche;

public:
    Rectangle();
    void setLongueur(int longueur);
    void setLargeur(int largeur);

    int getLongueur()const;
    int getLargeur()const;
    Point getCoinSuperieurGauche();

    int getPerimetre()const;
    int getSurface()const;

    bool getIsPlusGrandPerimetre(Rectangle rectangle)const;
    bool getIsPlusGrandSurface(Rectangle rectangle)const;

    void affiche();
};

#endif // RECTANGLE_H

#include <iostream>
#include "rectangle.h"
#include "Point.h"
#include <math.h>

using namespace std;

Rectangle::Rectangle()
{

}

int Rectangle::getLongueur()const{
    return this->longueur;
}

int Rectangle::getLargeur()const{
    return this->largeur;
}

Point Rectangle::getCoinSuperieurGauche(){
    this->coinSuperieurGauche.x=0;
    this->coinSuperieurGauche.y=this->largeur;
    return this->coinSuperieurGauche;
}

int Rectangle::getPerimetre()const{
    return 2*(this->longueur+this->largeur);
}

int Rectangle::getSurface()const{
    return this->longueur*this->largeur;
}

void Rectangle::setLongueur(int longueur){
    this->longueur=longueur;
}
void Rectangle::setLargeur(int largeur){
    this->largeur=largeur;
}

bool Rectangle::getIsPlusGrandPerimetre(Rectangle rectangle)const{
    if (this->getPerimetre()<rectangle.getPerimetre())
        return true;
    else
        return false;
}

bool Rectangle::getIsPlusGrandSurface(Rectangle rectangle)const{
    if (this->getSurface()<rectangle.getSurface())
        return true;
    else
        return false;
}

void Rectangle::affiche(){
    cout << "// ------ Rectangle ------ //" << endl;
    cout << "Longueur : " << getLongueur() << endl;
    cout << "Largeur : " << getLargeur() << endl;
    cout << "Point superieur gauche en x : " << getCoinSuperieurGauche().x << " et en y : " << getCoinSuperieurGauche().y << endl;
    cout << "Perimetre : " << getPerimetre() << endl;
    cout << "Surface : " << getSurface() << endl;

    // BONUS
    Rectangle monRectangle2 = Rectangle();
    monRectangle2.setLongueur(50);
    monRectangle2.setLargeur(50);

    cout << "(BONUS) Rectangle 2 possede un plus gand Perimetre (1:vrai/0:faux) : " << getIsPlusGrandPerimetre(monRectangle2) << endl;
    cout << "(BONUS) Rectangle 2 possede une plus gand Surface (1:vrai/0:faux) : " << getIsPlusGrandSurface(monRectangle2) << endl;

}


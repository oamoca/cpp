TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        cercle.cpp \
        main.cpp \
        rectangle.cpp \
        triangle.cpp

HEADERS += \
    Point.h \
    cercle.h \
    rectangle.h \
    triangle.h

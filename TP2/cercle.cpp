#include "cercle.h"
#include <iostream>
#include <math.h>

using namespace std;

Cercle::Cercle()
{

}

Point Cercle::getPointCentre()const{
    return this->point_centre;
}

int Cercle::getDiametre()const{
    return this->diametre;
}

void Cercle::setPointCentre(Point point_centre){
    this->point_centre=point_centre;
}

void Cercle::setDiametre(int diametre){
    this->diametre=diametre;
}

float Cercle::getPerimetre()const{
    return 3.14*getDiametre();
}

float Cercle::getSurface()const{
    return 3.14*pow((this->diametre/2), 2);
}

// Méthode get bool retourne vrai si le point est sur le cercle
bool Cercle::isPointOnCercle(Point point)const{
    bool pointIsOnCercle=false;
    // Calcul du point
    float equation = pow((point.x-this->point_centre.x),2) + pow((point.y-this->point_centre.y),2);

    // Test si le point est sur le cercle (égal au rayon²)
    if(equation == pow(this->diametre/2, 2)){
        pointIsOnCercle=true;
    }
    return pointIsOnCercle;
}

// Méthode get bool retourne vrai si le point est dans le cercle
bool Cercle::isPointInCercle(Point point)const{
    bool pointIsOnCercle=false;
    // Calcul du point
    float equation = pow((point.x-this->point_centre.x),2) + pow((point.y-this->point_centre.y),2);

    // Test si le point est dans le cercle (inférieur au rayon²)
    if(equation < pow(this->diametre/2, 2)){
        pointIsOnCercle=true;
    }
    return pointIsOnCercle;
}


void Cercle::affiche(){
    cout << "// ------ Cercle ------ //" << endl;
    cout << "Point du centre en x : " << getPointCentre().x << " et en y : " << getPointCentre().y << endl;
    cout << "Diametre : " << getDiametre() << endl;
    cout << "Perimetre : " << getPerimetre() << endl;
    cout << "Surface : " << getSurface() << endl;

    // Test si le point est sur le cercle
    Point pointSurCerlce = Point();
    pointSurCerlce.x=0;
    pointSurCerlce.y=5;
    cout << "Test si le point (" << pointSurCerlce.x << "," << pointSurCerlce.y << ") est sur le cercle (1:vrai/0:faux) : " << isPointOnCercle(pointSurCerlce) << endl;

    // Test si le point est dans le cercle
    Point pointDansCerlce = Point();
    pointDansCerlce.x=0;
    pointDansCerlce.y=0;
    cout << "Test si le point (" << pointDansCerlce.x << "," << pointDansCerlce.y << ") est dans le cercle (1:vrai/0:faux) : " << isPointInCercle(pointDansCerlce) << endl;

}


#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Point.h"


class Triangle
{

private:
    Point pointA, pointB, pointC ;

public:
    Triangle();

    Point getPointA()const;
    Point getPointB()const;
    Point getPointC()const;

    void afficher();
    void setPointA(Point pointA);
    void setPointB(Point pointB);
    void setPointC(Point pointC);
    float getBase()const;
    float getHauteur()const;
    float getSurface()const;
    float getLongueurs(Point pointA, Point pointB)const;

    bool getIsIsocele()const;
    bool getIsRectangle()const;
    bool getIsEquilateral()const;

    void affiche();
};

#endif // TRIANGLE_H

#include <iostream>
#include "cercle.h"
#include "triangle.h"
#include"Point.h"
#include "rectangle.h"

using namespace std;


int main()
{
    Rectangle monRectangle = Rectangle();
    monRectangle.setLongueur(20);
    monRectangle.setLargeur(10);
    monRectangle.affiche();


    Cercle monCercle = Cercle();
    Point pointCentre = Point();
    pointCentre.x = 0;
    pointCentre.y = 0;
    monCercle.setPointCentre(pointCentre);
    monCercle.setDiametre(10);
    monCercle.affiche();


    Triangle monTriangle = Triangle();
    // Coordonnées de point prit sur internet qui affiche un triangle isocèle et rectangle
    Point pointA = Point();
    pointA.x=-1;
    pointA.y=2;
    monTriangle.setPointA(pointA);
    Point pointB = Point();
    pointB.x=4;
    pointB.y=3;
    monTriangle.setPointB(pointB);
    Point pointC = Point();
    pointC.x=5;
    pointC.y=-2;
    monTriangle.setPointC(pointC);
    monTriangle.affiche();


    return 0;
}

#include "triangle.h"
#include <math.h>
#include <iostream>

using namespace std;

Triangle::Triangle()
{

}

Point Triangle::getPointA()const{
    return this->pointA;
}

Point Triangle::getPointB()const{
    return this->pointB;
}

Point Triangle::getPointC()const{
    return this->pointC;
}

void Triangle::setPointA(Point pointA){
    this->pointA=pointA;
}

void Triangle::setPointB(Point pointB){
    this->pointB=pointB;
}

void Triangle::setPointC(Point pointC){
    this->pointC=pointC;
}

// Méthode get float : base (plus grande longueur du triangle)
float Triangle::getBase()const{
    //A : pointA
    //B : pointB
    //C : pointC
    float AB = getLongueurs(pointA, pointB);
    float AC = getLongueurs(pointA, pointC);
    float BC = getLongueurs(pointB, pointC);

    float base=AB;
    if (AB>AC && AB >BC){
        base=AB;
    }
    else if(AC>AB && AC>BC){
        base=AC;
    }
    else if (AB==AC || AB==BC){
        base=AB;
    }
    else if (AC==AB || AC==BC){
        base=AC;
    }

    return base;
}

float Triangle::getHauteur()const{
    return (2*getSurface())/getBase();
}

float Triangle::getSurface()const{
    float AB = getLongueurs(pointA, pointB);
    float AC = getLongueurs(pointA, pointC);
    float BC = getLongueurs(pointB, pointC);

    return sqrt(((AB+AC+BC)/2)*(((AB+AC+BC)/2)-AB)*(((AB+AC+BC)/2)-AC)*(((AB+AC+BC)/2)-BC));
}

// Méthode get float : longueur entre deux point AB
float Triangle::getLongueurs(Point pointA, Point pointB)const{
    return sqrt(pow((pointA.x-pointB.x),2)+pow((pointA.y-pointB.y),2));
}

// Méthode get bool : return vrai si le triangle est isocèle
bool Triangle::getIsIsocele()const{
    float AB = getLongueurs(pointA, pointB);
    float AC = getLongueurs(pointA, pointC);
    float BC = getLongueurs(pointB, pointC);

    bool isIsocele=false;
    if (AB==AC || AB==BC){
        isIsocele=true;
    }
    else if (AC==AB || AC==BC){
        isIsocele=true;
    }
    return isIsocele;
}

// Méthode get bool : return vrai si le triangle est rectangle
bool Triangle::getIsRectangle()const{
    float hypothenuse = getBase();

    float AB = getLongueurs(pointA, pointB);
    float AC = getLongueurs(pointA, pointC);
    float BC = getLongueurs(pointB, pointC);

    float AB_carre = pow(AB, 2);
    float AC_carre = pow(AC, 2);
    float BC_carre = pow(BC, 2);
    float hypothenus_carre = pow(hypothenuse, 2);

    // On applicaue le theroreme de pythagore (ex : AC²=AB²+BC²)
    bool isRectangle=false;
    if((AB_carre+AC_carre) == hypothenus_carre && hypothenuse==BC){
        isRectangle=true;
    }
    if((BC_carre+AB_carre) == hypothenus_carre && hypothenuse==AC){
        isRectangle=true;
    }
    if(AC_carre+BC_carre == hypothenus_carre && hypothenuse==AB){
        isRectangle=true;
    }

    return isRectangle;
}

// Méthode get bool : return vrai si le triangle est equilateral
bool Triangle::getIsEquilateral()const{
    float AB = getLongueurs(pointA, pointB);
    float AC = getLongueurs(pointA, pointC);
    float BC = getLongueurs(pointB, pointC);

    bool isEquilateral=false;
    if (AB==AC && AB == BC && AC==BC){
        isEquilateral=true;
    }
    return isEquilateral;
}

void Triangle::affiche(){
    cout << "// ------ Triangle ------ //" << endl;
    cout << "Longueur AB : " << getLongueurs(pointA, pointB) << endl;
    cout << "Longueur AC : " << getLongueurs(pointA, pointC) << endl;
    cout << "Longueur BC : " << getLongueurs(pointB, pointC) << endl;

    cout << "Hauteur : " << getHauteur() << endl;
    cout << "Base : " << getBase() << endl;
    cout << "Surface : " << getSurface() << endl;
    cout << "Est isocele : " << getIsIsocele() << endl;
    cout << "Est rectangle : " << getIsRectangle() << endl;
    cout << "Est equilateral : " << getIsEquilateral() << endl;
}



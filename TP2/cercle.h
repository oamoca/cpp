#ifndef CERCLE_H
#define CERCLE_H
#include "Point.h"


class Cercle
{

private:
    Point point_centre;
    int diametre;

public:
    Cercle();

    Point getPointCentre()const;
    int getDiametre()const;
    float getPerimetre()const;
    float getSurface()const;

    void setPointCentre(Point point_centre);
    void setDiametre(int diametre);

    bool isPointOnCercle(Point point)const;
    bool isPointInCercle(Point point)const;

    void affiche();
};

#endif // CERCLE_H

#include "jeu.h"
#include "grillemorpion.h"
#include <iostream>
#include "grillepuissance4.h"

Jeu::Jeu(GrilleMorpion grille){
    grille = GrilleMorpion();
    isGrilleMorpion=true;
    joueurCourant=1;
    std::cout << "Tour du joueur : " << joueurCourant << std::endl;
}

Jeu::Jeu(GrillePuissance4 grilleP4){
    grilleP4 = GrillePuissance4();
    isGrilleMorpion=false;
    joueurCourant=1;
    std::cout << "Tour du joueur : " << joueurCourant << std::endl;
}

void Jeu::Start(){
    while(!finDePartie){
        // Affichage de la grille + test si la grille est pleine
        if(isGrilleMorpion){ // Pour la grille morpion
            grille.AfficherGrille();
            if(grille.GrilleComplete())
                FinDeGrille();
        }
        else{ // Pour la grille Puissance 4
            grilleP4.AfficherGrille();
            if(grilleP4.GrilleComplete())
                FinDeGrille();
        }

        // Saisie du joueur
        SaisieJoueur();

        // Test si il y a une victoire du joueur sinon tour suivant
        if(isGrilleMorpion){ // Pour la grille morpion
            if(grille.VictoireJoueur(joueurCourant)){
                grille.AfficherGrille();
                FinDeGrille();
            }
            else
                TourSuivant();
        }
        else{ // Pour la grille Puissance 4
            if(grilleP4.VictoireJoueur(joueurCourant)){
                grilleP4.AfficherGrille();
                FinDeGrille();
            }
            else
                TourSuivant();
        }
    }
}

void Jeu::TourSuivant(){
    if(this->joueurCourant==1)
        this->joueurCourant=2;
    else
        this->joueurCourant=1;
    std::cout << "Tour du joueur : " << joueurCourant << std::endl;
}

void Jeu::FinDeGrille(){
    if(isGrilleMorpion){
        if(grille.GrilleComplete())
            std::cout << "Egalite !" << std::endl;
        else
            std::cout << "Le joueur " << this->joueurCourant << " a gagne la partie !" << std::endl;
    }
    else{
        if(grilleP4.GrilleComplete())
            std::cout << "Egalite !" << std::endl;
        else
            std::cout << "Le joueur " << this->joueurCourant << " a gagne la partie !" << std::endl;
    }

    int choixJoueur=-1;
    finDePartie=false;

    while (choixJoueur!=0 && choixJoueur!= 1){
        std::cout << "Souhaitez vous recommencer une partie (0 : non / 1 : oui) ? ";
        std::cin >> choixJoueur;

        if(std::cin.fail()){ // Si l'utilisateur n'a pas mit de chiffre
            std::cin.clear(); // reset cin
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip cin

            std::cout << "Veuillez choisir un chiffre entre 0 et 1 !" << std::endl;
        }
        else{
            if(choixJoueur==0){
                finDePartie=true;
            }
            else if (choixJoueur==1){
                finDePartie=false;
                if(isGrilleMorpion)
                    grille.ReinitialiserGrille();
                else
                    grilleP4.ReinitialiserGrille();
                std::cout << "Tour du joueur : " << joueurCourant << std::endl;
            }
            else{
                std::cout << "Veuillez chosir un chiffre entre 0 et 1 !" << std::endl;
            }
        }
    }
}

void Jeu::SaisieJoueur(){
    int ligneJoueur=0;
    int colonneJoueur=0;
    bool colonneJouable=false;
    int i=0;

    if(isGrilleMorpion){ // Pour la grille morpion
        while (ligneJoueur<1 || ligneJoueur>3 || colonneJoueur<1 || colonneJoueur>3){
            std::cout << "Saisissez une ligne a jouer (1 a 3) : ";
            std::cin >> ligneJoueur;

            std::cout << "Saisissez une colonne a jouer (1 a 3) : ";
            std::cin >> colonneJoueur;

            if(std::cin.fail()){ // Si l'utilisateur n'a pas mit de chiffre
                std::cin.clear(); // reset cin
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip cin

                std::cout << "Veuillez entrer un chiffre compris entre 1 et 3 !" << std::endl;
            }
            else{ // Test si la case est vide
                if(ligneJoueur>=1 && ligneJoueur<=3 && colonneJoueur>=1 && colonneJoueur<=3){
                    if(grille.CaseVide(ligneJoueur-1, colonneJoueur-1)){
                        grille.DeposerUnJeton(ligneJoueur-1, colonneJoueur-1, joueurCourant);
                    }
                    else
                        std::cout << "Case deja rempli !" << std::endl;
                }
                else
                    std::cout << "Vous n'avez pas saisie les valeurs dans l'intervalle !" << std::endl;
            }
        }
    }
    else{ // Pour la grille Puissance 4
        while (colonneJoueur<1 || colonneJoueur>7 || colonneJouable==false){
            std::cout << "Saisissez une colonne a jouer (1 a 7) : ";
            std::cin >> colonneJoueur;

            if(std::cin.fail()){ // Si l'utilisateur n'a pas mit de chiffre
                std::cin.clear(); // reset cin
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip cin

                std::cout << "Veuillez entrer un chiffre compris entre 1 et 7 !" << std::endl;
            }
            else{ // Test si la colonne est rempli
                if(colonneJoueur>=1 && colonneJoueur<=7){
                    for (i=3; i>=0; i--){
                        if(grilleP4.CaseVide(i, colonneJoueur-1)){
                            colonneJouable=true;
                            break;
                        }
                    }
                    if(colonneJouable) // Si elle n'est pas rempli on peut deposer le jeton
                        grilleP4.DeposerUnJeton(i, colonneJoueur-1, joueurCourant);
                    else
                        std::cout << "Colonne deja rempli !" << std::endl;
                }
                else
                    std::cout << "Vous n'avez pas saisie les valeurs dans l'intervalle !" << std::endl;
            }
        }
    }
}

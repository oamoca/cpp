#ifndef JEU_H
#define JEU_H
#include "grillemorpion.h"
#include "grillepuissance4.h"


class Jeu
{
private:
    GrilleMorpion grille;
    GrillePuissance4 grilleP4;
    int joueurCourant;
    bool finDePartie=false;
    bool isGrilleMorpion=true;

public:
    Jeu(GrilleMorpion grille);
    Jeu(GrillePuissance4 grilleP4);
    void TourSuivant();
    void SaisieJoueur();
    void Start();
    void FinDeGrille();
};

#endif // JEU_H

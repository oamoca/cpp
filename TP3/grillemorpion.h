#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H
#include "case.h"
#include <array>

class GrilleMorpion
{
private:
    std::array<std::array<Case,3>,3> morpion_tableau;

public:
    GrilleMorpion();
    bool CaseVide(int ligne, int colonne);
    void DeposerUnJeton(int ligne, int colonne, int idJoueur);
    bool LigneComplete(int ligne, int idJoueur);
    bool ColonneComplete(int colonne, int idJoueur);
    bool DiagonaleComplete(int diagonale, int idJoueur);
    bool VictoireJoueur(int idJoueur);
    void AfficherGrille();
    void ReinitialiserGrille();
    bool GrilleComplete();
};

#endif // GRILLEMORPION_H

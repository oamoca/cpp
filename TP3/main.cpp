#include <iostream>
#include "grillemorpion.h"
#include "jeu.h"
#include "grillepuissance4.h"

using namespace std;

int main()
{
    int choixGrille=-1;
    while (choixGrille != 0 && choixGrille != 1){ // Demande a l'utilisateur le mode de jeu
        std::cout << "Choisissez le mode de jeu ( 0 : Morpion / 1 : Puissance 4 ) : ";
        std::cin >> choixGrille;

        if(std::cin.fail()){ // Si l'utilisateur n'a pas mit de chiffre
            std::cin.clear(); // reset cin
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip cin

            std::cout << "Veuillez entrer un chiffre compris entre 0 et 1 !" << std::endl;
            choixGrille=-1;
        }
        else{
            if(choixGrille==0){
                GrilleMorpion grille;
                Jeu jeu = Jeu(grille);
                jeu.Start();
            }
            else if (choixGrille==1){
                GrillePuissance4 grilleP4;
                Jeu jeuP4 = Jeu(grilleP4);
                jeuP4.Start();
            }
            else
                std::cout << "Veuillez entrer un chiffre compris entre 0 et 1 !" << std::endl;
        }
    }
    return 0;
}

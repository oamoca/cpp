#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H
#include "case.h"
#include <array>


class GrillePuissance4
{
private:
    std::array<std::array<Case,7>,4> p4_tableau;

public:
    GrillePuissance4();
    bool CaseVide(int ligne, int colonne);
    void DeposerUnJeton(int ligne, int colonne, int idJoueur);
    bool LigneComplete(int ligne, int idJoueur);
    bool ColonneComplete(int colonne, int idJoueur);
    bool DiagonaleComplete(int diagonale, int idJoueur);
    bool VictoireJoueur(int idJoueur);
    void AfficherGrille();
    void ReinitialiserGrille();
    bool GrilleComplete();
};

#endif // GRILLEPUISSANCE4_H

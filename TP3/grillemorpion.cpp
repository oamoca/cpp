#include "grillemorpion.h"
#include "case.h"
#include <iostream>


GrilleMorpion::GrilleMorpion()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j){
            this->morpion_tableau[i][j]=Case(0);
        }
    }
}

bool GrilleMorpion::CaseVide(int ligne, int colonne){
    if (this->morpion_tableau[ligne][colonne].GetCase()==0)
        return true;
    else
        return false;
}

void GrilleMorpion::DeposerUnJeton(int ligne, int colonne, int idJoueur){
    this->morpion_tableau[ligne][colonne].SetCase(idJoueur);
}

bool GrilleMorpion::LigneComplete(int ligne, int idJoueur){
    bool isLigneComplete=true;
    for (int j = 0; j < 3; ++j){
        if(this->morpion_tableau[ligne][j].GetCase()!=idJoueur)
            isLigneComplete=false;
    }
    return isLigneComplete;
}

bool GrilleMorpion::ColonneComplete(int colonne, int idJoueur){
    bool isColonneComplete=true;
    for (int i = 0; i < 3; ++i) {
        if(this->morpion_tableau[i][colonne].GetCase() != idJoueur)
            isColonneComplete=false;
    }
    return isColonneComplete;
}

bool GrilleMorpion::DiagonaleComplete(int diagonale, int idJoueur){
    bool isDiagonaleComplete = true;
    if (morpion_tableau[1][1].GetCase() != idJoueur)
        isDiagonaleComplete = false;
    if (diagonale == 0){
        if (morpion_tableau[0][0].GetCase() != idJoueur || morpion_tableau[2][2].GetCase() != idJoueur )
            isDiagonaleComplete = false;
    }
    else if (diagonale == 2){
        if ( morpion_tableau[2][0].GetCase() != idJoueur || morpion_tableau[0][2].GetCase() != idJoueur)
            isDiagonaleComplete = false;
    }
    return isDiagonaleComplete;
}

void GrilleMorpion::AfficherGrille(){
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j){
            if (j==0)
                std::cout << "| ";
            std::cout << this->morpion_tableau[i][j].GetCase() << " | ";
        }
        std::cout << std::endl;
    }
}

bool GrilleMorpion::VictoireJoueur(int idJoueur){
    return (LigneComplete(0,idJoueur) || LigneComplete(1,idJoueur) || LigneComplete(2,idJoueur)
                || ColonneComplete(0,idJoueur) || ColonneComplete(1,idJoueur) || ColonneComplete(2,idJoueur)
                || DiagonaleComplete(0,idJoueur) || DiagonaleComplete(2,idJoueur));
}

void GrilleMorpion::ReinitialiserGrille(){
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j){
            this->morpion_tableau[i][j]=Case(0);
        }
    }
}

bool GrilleMorpion::GrilleComplete(){
    bool existCaseVide=true;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j){
            if(CaseVide(i, j))
                existCaseVide=false;
        }
    }
    return existCaseVide;
}

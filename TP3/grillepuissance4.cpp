#include "grillepuissance4.h"
#include "case.h"
#include <iostream>

GrillePuissance4::GrillePuissance4()
{
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 7; ++j){
            this->p4_tableau[i][j]=Case(0);
        }
    }
}

bool GrillePuissance4::CaseVide(int ligne, int colonne){
    if (this->p4_tableau[ligne][colonne].GetCase()==0)
        return true;
    else
        return false;
}

void GrillePuissance4::DeposerUnJeton(int ligne, int colonne, int idJoueur){
    this->p4_tableau[ligne][colonne].SetCase(idJoueur);
}

bool GrillePuissance4::LigneComplete(int ligne, int idJoueur){
    bool isLigneComplete=false;
    int suite=0;
    for (int j = 0; j < 7 && suite<4; ++j){
        if(this->p4_tableau[ligne][j].GetCase()==idJoueur)
            suite++;
        else
            suite=0;
    }
    if (suite>=4)
        isLigneComplete=true;
    return isLigneComplete;
}

bool GrillePuissance4::ColonneComplete(int colonne, int idJoueur){
    bool isColonneComplete=false;
    int suite=0;
    for (int i = 0; i < 4 && suite<4; ++i) {
        if(this->p4_tableau[i][colonne].GetCase()==idJoueur)
            suite++;
        else
            suite=0;
    }
    if (suite>=4)
        isColonneComplete=true;

    return isColonneComplete;
}

bool GrillePuissance4::DiagonaleComplete(int diagonale, int idJoueur){
    bool isDiagonaleComplete=false;

    // Diagonale Decroissant
    int suiteDecroissant=0;
    for(int i=diagonale; i<diagonale+4;i++){
        if(p4_tableau[0][i].GetCase()==idJoueur && i==diagonale)
            suiteDecroissant++;
        else if (p4_tableau[1][i].GetCase()==idJoueur && i==diagonale+1)
            suiteDecroissant++;
        else if (p4_tableau[2][i].GetCase()==idJoueur && i==diagonale+2)
            suiteDecroissant++;
        else if (p4_tableau[3][i].GetCase()==idJoueur && i==diagonale+3)
            suiteDecroissant++;
    }
    if (suiteDecroissant==4)
        isDiagonaleComplete=true;

    // Diagonale Croissant
    int suiteCroissant=0;
    for(int i=diagonale; i<diagonale+4;i++){
        if(p4_tableau[3][i].GetCase()==idJoueur && i==diagonale)
            suiteCroissant++;
        else if (p4_tableau[2][i].GetCase()==idJoueur && i==diagonale+1)
            suiteCroissant++;
        else if (p4_tableau[1][i].GetCase()==idJoueur && i==diagonale+2)
            suiteCroissant++;
        else if (p4_tableau[0][i].GetCase()==idJoueur && i==diagonale+3)
            suiteCroissant++;
    }
    if (suiteCroissant==4)
        isDiagonaleComplete=true;

    return isDiagonaleComplete;
}

void GrillePuissance4::AfficherGrille(){
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 7; ++j){
            if (j==0)
                std::cout << "| ";
            std::cout << this->p4_tableau[i][j].GetCase() << " | ";
        }
        std::cout << std::endl;
    }
}

bool GrillePuissance4::VictoireJoueur(int idJoueur){
    return (LigneComplete(0,idJoueur) || LigneComplete(1,idJoueur) || LigneComplete(2,idJoueur) || LigneComplete(3,idJoueur)
            || ColonneComplete(0,idJoueur) || ColonneComplete(1,idJoueur) || ColonneComplete(2,idJoueur) || ColonneComplete(3,idJoueur)
            || ColonneComplete(4,idJoueur) || ColonneComplete(5,idJoueur) || ColonneComplete(6,idJoueur)
            || DiagonaleComplete(0, idJoueur) || DiagonaleComplete(1, idJoueur) || DiagonaleComplete(2, idJoueur)
            || DiagonaleComplete(3, idJoueur));
}

void GrillePuissance4::ReinitialiserGrille(){
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 7; ++j){
            this->p4_tableau[i][j]=Case(0);
        }
    }
}

bool GrillePuissance4::GrilleComplete(){
    bool existCaseVide=true;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 7; ++j){
            if(CaseVide(i, j))
                existCaseVide=false;
        }
    }
    return existCaseVide;
}

#include <iostream>
#include <string>
#include <random>


using namespace std;

void majusculeNomPrenom(string str){
    int debut = 0;
    int fin = str.find(' ');
    string prenom;
    while (fin != -1) {
        prenom = str.substr(debut, fin - debut);
        debut = fin + 1;
        fin = str.find(' ', debut);
    }

    string nom = str.substr(debut, fin - debut);
    for (auto & c: nom) c = toupper(c);
    prenom[0]=toupper(prenom[0]);

    cout << "Bonjour " << prenom << " " << nom << " !" << endl;
}

// --- 3.1.1 --- 
void demandeNomPrenom(){
    string prenom;
    string nom;
    cout << "Renseignez maintenant votre nom et prénom séparemment." << endl;
    cout << "Tapper votre prénom : ";
    cin >> prenom;
    cout << "Tapper votre nom : ";
    cin >> nom;
    prenom[0]=toupper(prenom[0]);
    for (auto & c: nom) c = toupper(c);
    cout << "Bonjour " << prenom << " " << nom << " !" << endl;
}

// --- 3.1.2 ---
void demandeNomPrenomEnsemble(){
    char nom_prenom[100];
    cout << "Tapper votre PRENOM et NOM : ";
    cin.getline(nom_prenom,sizeof(nom_prenom));

    majusculeNomPrenom(nom_prenom);
}

// --- 3.2.1 ------ 3.2.2 ---
void devinerNbAleatoire(){
    int nbAlea = rand() % 1000;
    cout << "Un nombre aléatoire a était généré entre 0 et 1000, essayez de le deviner." << endl;

    int nbUser;
    while (nbAlea != nbUser){
        cout << "Sasissez un nombre : ";
        cin >> nbUser;

        if(nbUser < nbAlea){
            cout << "Le nombre est plus grand !" << endl;
        }
        else if (nbUser > nbAlea){
            cout << "Le nombre est plus petit !" << endl;
        }
        else{
            cout << "Bravo ! Le nombre était bien : " << nbAlea << endl;
        }
    }
}

// --- 3.2.3 --- (BONUS)
void devinerNbAleatoireTerminal(){
    
    cout << "Le terminal va essayer de deviner votre nombre compris entre 0 et 1000." << endl;
    int borneMin =0;
    int borneMax=1000;
    int nbInitAlea = rand() % 1000;
    int nbUser=-1;
    int milieu;
    int index=1;

    while (nbUser!=0){
        milieu = (borneMax-borneMin)/2 + borneMin;
        cout << "Le nombre est-il " << milieu << " ? (1:Plus grand / 2:Plus petit / 0:Juste)" <<endl;

        cin >> nbUser;

        if(nbUser==1){
            borneMin=milieu;
        }
        else if (nbUser==2){
            borneMax=milieu;
        }
        else if (nbUser==0){
            cout << "Le terminal à trouvé le nombre " << milieu << " en " << index << " coups." << endl;
        }
        index++;
    }
}

int main() {
    
    // --- 3.1.2 ---
    demandeNomPrenomEnsemble();
    cout << "\n";

    // --- 3.1.1 --- 
    demandeNomPrenom();
    cout << "\n";
    
    // --- 3.2.1 ------ 3.2.2 ---
    devinerNbAleatoire();
    cout << "\n";

    // --- 3.2.3 (BONUS)
    devinerNbAleatoireTerminal();

    return 0;
}
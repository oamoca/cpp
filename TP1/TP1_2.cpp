#include <iostream>

using namespace std;

void resultatScore(int nbEchangeJ1, int nbEchangeJ2){
    if (nbEchangeJ1 > nbEchangeJ2){
        if ((nbEchangeJ1 - nbEchangeJ2)>=2){
            if (nbEchangeJ1!=4 || nbEchangeJ2 != 3){
                cout << "Le joueur 1 à l'avantage" << endl;
            }
            else{
                cout << "Le joueur 1 gagne la partie !" << endl;
            }
        }
        else if ((nbEchangeJ1 - nbEchangeJ2)==1){
            cout << "Le joueur 1 à l'avantage" << endl;
        }
    }
    else if (nbEchangeJ1 < nbEchangeJ2){
        if ((nbEchangeJ2 - nbEchangeJ1)>=2){
            if(nbEchangeJ2 !=4 || nbEchangeJ2!=3){
                cout << "Le joueur 2 à l'avantage" << endl;
            }
            else{
                cout << "Le joueur 2 gagne la partie !" << endl;
            }
        }
        else if ((nbEchangeJ2 - nbEchangeJ1)==1){
            cout << "Le joueur 2 à l'avantage" << endl;
        }
    }
    else {
        cout << "Les deux joueurs sont à égalités" << endl;
    }
}

string calculScoreJoueur(int echangeJoueur){
    string scoreJoueur;
    switch (echangeJoueur)
    {
        case 1:
            scoreJoueur="15";
            break;
        case 2:
            scoreJoueur="30";
            break;
        case 3:
            scoreJoueur="40";
            break;
        case 4:
            scoreJoueur="40+A";
            break;
        default:
            scoreJoueur="0";
            break;
    }
    return scoreJoueur;
}

int main(){

    int nbEchangeJ1;
    int nbEchangeJ2;

    cout << "Tappez le nombre d'échanges gagné pour chaque joueur." << endl;
    cout << "Joueur 1 : ";
    cin >> nbEchangeJ1;
    cout << "Joueur 2 : ";
    cin >> nbEchangeJ2;

    string scoreJoueurA = calculScoreJoueur(nbEchangeJ1);
    string scoreJoueurB = calculScoreJoueur(nbEchangeJ2);

    cout << "\nLe joueur 1 à " << scoreJoueurA << " points" << endl;
    cout << "Le joueur 2 à " << scoreJoueurB << " points\n" << endl;

    resultatScore(nbEchangeJ1, nbEchangeJ2);
}
#include <iostream>
#include <random>

using namespace std;

int somme(int a, int b){
  return a+b;
}

void inverseValeur(int &a, int &b){
    int valA = a;
    int valB = b;
    a = valB;
    b = valA;
}

int remplace3eValeur(int a, int b, int c){
  c=a+b;
  return c;
}

int remplace3eValeurPointeur(int a, int b, int c){
  c=a+b;
  return c;
}

void remplacePointeur(int a, int b, int *c){
    *c = a + b;
}

constexpr int MAX = 100;

int main() {

  // --- 1.1.1 ---
  std::cout << "Somme de 5+6 : " << somme(5, 6) << endl;

  // --- 1.1.2 ---
  int a = 5;
  int b = 7;
  inverseValeur(a, b);
  std::cout << "Valeur inversée de a=5 et b=7 :  a=" << a << " b=" << b << endl;

  // --- 1.1.3 --- (1ère partie)
  std::cout << "Remplace la 3e valeur : " << remplace3eValeur(2, 5, 8) << endl;

  // --- 1.1.3 --- (2e partie)
  int c=9;
  remplacePointeur(7, 3, &c);
  cout << "Remplace la 3e valeur (pointeur) : " << c << endl;

  // --- 1.1.4 ---
  // Bonus 1 :
  int tailleTab;
  cout << "Saisir la taille du tableau : " << endl;
  cin >> tailleTab;

  //Rempli le tableau avec des valeurs aléatoires et affiche le tableau (non trié)
  int tab[tailleTab];
  std::cout << "\nTaille du tableau : " << sizeof(tab)/sizeof(tab[0]) << "\nLes valeurs aléatoires positifs sont limitée à : " << MAX << endl;
  for (int i=0;i<(sizeof(tab)/sizeof(tab[0])); i++){
    tab[i]=rand() % MAX;
    std::cout << "Valeur "<< i << " : " << tab[i]<< endl;
  }
  //cout << sizeof(tab) << " " << sizeof(tab[0]) << endl;

  // Bonus 2 : 
  int choixTri = 1;
  cout << "Trier le tableau par ordre croissant (1) ou décroissant (2) ? (1/2)" << endl;
  cin >> choixTri;

  string choixTriStr = "croissant";
  if (choixTri==1){
    choixTriStr = "croissant";
    //Tri le tableau par ordre croissant
    for (int a = 0; a < (sizeof(tab)/sizeof(tab[0])); a++){
      for (int b = 0; b < (sizeof(tab)/sizeof(tab[0])); b++){
        if (tab[b]>tab[a]){
          int temp = tab[a];
          tab[a] = tab[b];
          tab[b] = temp;
        }  
      }
    }
  }
  else if (choixTri==2){
    choixTriStr = "décroissant";
    //Tri le tableau par ordre décroissant
    for (int a = 0; a < (sizeof(tab)/sizeof(tab[0])); a++){
      for (int b = 0; b < (sizeof(tab)/sizeof(tab[0])); b++){
        if (tab[b]<tab[a]){
          int temp = tab[a];
				  tab[a] = tab[b];
				  tab[b] = temp;
        }  
      }
	  }
  }
  else{
    cout << "Vous avez saisie un mauvais nombre ! La tri par défaut sera par ordre croissant." << endl;
  }

  //Affiche le tableau trié
  std::cout << "\nTableau trié par ordre " << choixTriStr << " :" << endl;
  for (int i=0;i<(sizeof(tab)/sizeof(tab[0])); i++){
    std::cout << "Valeur "<< i << " : " << tab[i]<< endl;
  }

  return 0;
}